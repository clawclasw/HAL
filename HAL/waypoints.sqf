HAL_MAX_ATTEMPTS_MULTIPLIER = 10;

HAL_waypoints_fnc_createWaypoint = {
	params [
		"_grp",
		"_position",
		["_behaviour", "AWARE"],
		["_combatMode", "RED"],
		["_formation", "WEDGE"],
		["_speed", "NORMAL"],
		["_type", "MOVE"],
		["_timeout", [0, 0, 0]]
	];

	private _wp = _grp addWaypoint [_position, 0];
	_wp setWaypointBehaviour _behaviour;
	_wp setWaypointCombatMode _combatMode;
	_wp setWaypointFormation _formation;
	_wp setWaypointSpeed _speed;
	_wp setWaypointType _type;
	_wp setWaypointTimeout _timeout;

_wp
};

HAL_waypoints_fnc_createCampWaypoint = {
	params ["_grp", "_position"];

	[
		_grp,
		_position,
		"AWARE",
		"RED",
		selectRandom HAL_VALID_FORMATIONS,
		"FULL",
		"MOVE",
		[0, 0, 0]
	] call HAL_waypoints_fnc_createWaypoint
};

HAL_waypoints_fnc_createSadWaypoint = {
	params ["_grp", "_position"];

	[
		_grp,
		_position,
		"AWARE",
		"RED",
		selectRandom HAL_VALID_FORMATIONS,
		"FULL",
		"SAD",
		[0, 0, 0]
	] call HAL_waypoints_fnc_createWaypoint
};

HAL_waypoints_fnc_createPatrolWaypoint = {
	params ["_grp", "_position"];

	[
		_grp,
		_position,
		"AWARE",
		"RED",
		selectRandom HAL_VALID_FORMATIONS,
		"NORMAL",
		"MOVE",
		[60, 120, 180]
	] call HAL_waypoints_fnc_createWaypoint
};

HAL_waypoints_fnc_createCycleWaypoint = {
	params ["_grp", "_position"];

	[
		_grp,
		_position,
		"AWARE",
		"RED",
		selectRandom HAL_VALID_FORMATIONS,
		"NORMAL",
		"CYCLE",
		[60, 120, 180]
	] call HAL_waypoints_fnc_createWaypoint
};

HAL_waypoints_fnc_findEmptySpotInZone = {
	params ["_zone", "_chosenSpots"];

	private _blacklistedPositions = [];
	{ _blacklistedPositions pushBack [_x, 25] } forEach _chosenSpots;

	private _unsafeSpot = [
		[_zone],
		_blacklistedPositions
	] call BIS_fnc_randomPos;

	private _safeSpot = [
		_unsafeSpot,
		0,
		25,
		5,
		0,
		1,
		0,
		_blacklistedPositions,
		[[0, 0], [0, 0]]
	] call BIS_fnc_findSafePos;

_safeSpot
};

HAL_waypoints_fnc_generateWaypoints = {
	params ["_trigger", "_number"];

	private _nAttempts = 0;
	private _maxAttempts = _number * HAL_MAX_ATTEMPTS_MULTIPLIER;
	private _waypoints = [];
	while {_nAttempts < _maxAttempts && {count _waypoints < _number}} do {
		private _spot = [_trigger, _waypoints] call HAL_waypoints_fnc_findEmptySpotInZone;
		
		if (!(_spot isEqualTo [0, 0])) then {
			_waypoints pushBackUnique _spot;
		};

		_nAttempts = _nAttempts + 1;
	};

	if (count _waypoints != _number) exitWith {
		systemChat format["ERROR: Could not find enough waypoints in zone %1 (%2 < %3).", _trigger, count _waypoints, _number];
		[]
	};

_waypoints
};

HAL_waypoints_fnc_pointIsTooCloseToOtherPoints = {
	params ["_point", "_otherPoints", "_maxDistance"];

	private _tooCloseToOtherPoints = false;
	{
		if (_point distance _x < _maxDistance) exitWith {
			_tooCloseToOtherPoints = true;
		};
	} forEach _otherPoints;

_tooCloseToOtherPoints
};

HAL_waypoints_fnc_findBuildingPositions = {
	params ["_building"];

	private _positions = [_building] call BIS_fnc_buildingPositions;
	private _validPositions = [];
	{
		if (
			// Nothing blocking this position, at around eye level.
			!(lineIntersects [ATLtoASL [_x #0, _x #1, _x #2 + 2], ATLtoASL [_x #0, _x #1, _x #2 + 2]])
			// No points too close to another point.
			&& {!([_x, _validPositions, 3] call HAL_waypoints_fnc_pointIsTooCloseToOtherPoints)}
		) then {
			_validPositions pushBack _x;
		};
	} forEach _positions;

_validPositions
};