HAL_debug_fnc_isMarkerCreated = {
	params ["_name"];

getMarkerColor _name != ""
};

HAL_debug_fnc_updateLineMarker = {
	params ["_name", "_pointA", "_pointB", ["_color", "ColorBlack"]];

	// Drawing a perfect line from point to point looks a little weird.
	// Give it a few meters gap.
	private _lineLeeway = 5;

	private _midPoint = [
		((_pointA select 0) + (_pointB select 0)) / 2,
		((_pointA select 1) + (_pointB select 1)) / 2
	];
	
	if (!([_name] call HAL_debug_fnc_isMarkerCreated)) then {
		createMarkerLocal [_name, _midPoint];
		_name setMarkerShapeLocal "RECTANGLE";
		_name setMarkerBrushLocal "Solid";
		_name setMarkerColorLocal _color;
		_name setMarkerAlphaLocal 0.6;
	};

	_name setMarkerPosLocal _midPoint;
	_name setMarkerSizeLocal [5, ((_pointA distance2D _pointB) / 2) - (_lineLeeway * 2)];
	_name setMarkerDirLocal (_pointA getDir _pointB);
};

HAL_debug_fnc_updateBrushMarker = {
	params ["_name", "_pos", ["_size", [100, 100]], ["_angle", 0], ["_shape", "ELLIPSE"], ["_type", "Solid"], ["_color", "ColorBlack"]];

	if (!([_name] call HAL_debug_fnc_isMarkerCreated)) then {
		createMarkerLocal [_name, _pos];
		_name setMarkerShapeLocal _shape;
		_name setMarkerBrushLocal _type;
		_name setMarkerColorLocal _color;
		_name setMarkerSizeLocal _size;
		_name setMarkerDirLocal _angle;
		_name setMarkerAlphaLocal 0.4;
	};

	_name setMarkerPosLocal _pos;
};

HAL_debug_fnc_updateIconMarker = {
	params ["_name", "_pos", ["_type", "mil_dot"], ["_color", "ColorBlack"], ["_text", ""]];

	if (!([_name] call HAL_debug_fnc_isMarkerCreated)) then {
		createMarkerLocal [_name, _pos];
		_name setMarkerTypeLocal _type;
		_name setMarkerColorLocal _color;
		_name setMarkerAlphaLocal 0.6;
	};

	_name setMarkerPosLocal _pos;
	_name setMarkerTextLocal _text;
};

HAL_debug_fnc_updateGroupMarkers = {
	params ["_grp"];

	private _uniqueGrpId = _grp getVariable ["HAL_groupId", format["%1_%2", side (leader _grp), groupId _grp]];

	private _grpMkrName = format["HAL_debug_marker_%1", _uniqueGrpId];
	private _grpWpLineMkrName = format["HAL_debug_marker_wp_line_%1", _uniqueGrpId];
	
	private _leaderPos = position (leader _grp);
	private _unitCount = {alive _x} count (units _grp);
	private _color = [side (leader _x), true] call BIS_fnc_sideColor;
	private _text = format["%1: %2", _unitCount, behaviour (leader _grp)];

	if (_unitCount < 1) then {
		_grpMkrName setMarkerAlphaLocal 0;
		_grpWpLineMkrName setMarkerAlphaLocal 0;
	} else {
		// Group marker.
		[_grpMkrName, _leaderPos, "o_inf", _color, _text] call HAL_debug_fnc_updateIconMarker;

		// Waypoint.
		private _currentWp = [_grp, currentWaypoint _grp];
		private _currentWpPos = getWPPos _currentWp;

		if (!(_currentWpPos isEqualTo [0, 0, 0])) then {
			[_grpWpLineMkrName, _leaderPos, _currentWpPos, _color] call HAL_debug_fnc_updateLineMarker;
		};
	};
};

HAL_debug_fnc_getZoneMarkerBrush = {
	params ["_type"];

	if (_type == "PATROL") exitWith { "SolidBorder" };
	if (_type == "CAMP") exitWith { "Cross" };
	if (_type == "GARRISON") exitWith { "FDiagonal" };
};

HAL_debug_fnc_updateZoneMarker = {
	params ["_trigger"];

	private _radius = (triggerArea _trigger) select 0;

	private _mkrName = format["HAL_debug_marker_zone_%1", _trigger];

	private _zoneType = _trigger getVariable "HAL_zone_type";
	private _brush = [_zoneType] call HAL_debug_fnc_getZoneMarkerBrush;
	private _area = triggerArea _trigger;

	_area params ["_a", "_b", "_angle", "_isRectangle"];

	if (_isRectangle) then {
		[_mkrName, position _trigger, [_a, _b], _angle, "RECTANGLE", _brush] call HAL_debug_fnc_updateBrushMarker;
	} else {
		[_mkrName, position _trigger, [_a, _b], _angle, "ELLIPSE", _brush] call HAL_debug_fnc_updateBrushMarker;
	};
};

HAL_debug_fnc_getAllAiGroups = {
	private _groups = [];
	{
		if (!isNull _x && {simulationEnabled (leader _x)} && {!isPlayer (leader _x)}) then {
			_groups pushBack _x;
		};
	} forEach allGroups;

_groups
};

HAL_debug_fnc_getAllZones = {
	private _zones = [];
	{
		private _zoneType = _x getVariable ["HAL_zone_type", ""];
		if (_zoneType != "") then {
			_zones pushBack _x;
		};
	} forEach (allMissionObjects "EmptyDetector");

_zones
};

HAL_debug_fnc_aiMarkerLoop = {
	HAL_debug_disabled = false;

	// Run until HAL_debug_disabled is set to true.
	waitUntil {
		{
			[_x] call HAL_debug_fnc_updateGroupMarkers;
		} forEach (call HAL_debug_fnc_getAllAiGroups);

		{
			[_x] call HAL_debug_fnc_updateZoneMarker;
		} forEach (call HAL_debug_fnc_getAllZones);

		sleep 1;
	
	HAL_debug_disabled
	};

	// Cleanup remaining markers.
	{
		if (["HAL_debug_marker", _x] call BIS_fnc_inString) then {
			deleteMarkerLocal _x;
		};
	} forEach allMapMarkers;
};