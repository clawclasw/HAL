// Cooldown on each group's radio reports, in seconds.
// Only triggers when a group reports a contact.
HAL_RADIO_COOLDOWN_SECONDS = 120;

// Distance from radio reports at which groups are set to COMBAT behaviour.
HAL_COMBAT_DISTANCE = 250;

// Distance from radio reports at which group leaders are notified of contacts.
HAL_RADIO_DISTANCE = 500;

HAL_radis_fnc_getEstimatedTargetPosition = {
	params ["_target", "_accuracy"];

[ [ [position _target, _accuracy] ], []] call BIS_fnc_randomPos
};

HAL_radio_fnc_nearestInactivePatrolGroup = {
	params ["_position"];

	private _nearestGroup = [99999, objNull];
	{
		private _leaderPos = position (leader _x);
		private _distanceToGroup = _position distance2D _leaderPos;
		private _groupType = _x getVariable ["HAL_groupType", "NONE"];
		if (
			_distanceToGroup < HAL_RADIO_DISTANCE
			&& {_groupType == "PATROL"}
			&& {_distanceToGroup < (_nearestGroup #0)}
			&& {behaviour (leader _x) != "COMBAT"}
		) then {
			_nearestGroup = [_distanceToGroup, _x];
		};
	} forEach allGroups;

_nearestGroup #1
};

HAL_radio_fnc_firedNearHandler = {
	params ["_leader", "_firer", "_distance", "_weapon", "_muzzle", "_mode", "_ammo", "_gunner"];
	
	// Only consider shots by friendlies. If we don't fire at them, we didn't have time to radio in.
	if (side _firer != side _leader) exitWith { };

	// Then, make sure it's in our group. We only want to report shots on targets by our own group.
	if (group _firer != group _leader) exitWith { };

	// Finally, check the cooldown on radio reports to avoid performance hits.
	if ((group _leader) getVariable ["HAL_radio_lastReport", -9999] > (time - HAL_RADIO_COOLDOWN_SECONDS)) exitWith { };

	// Take the unit that fired a shot and get a list of all known targets from the last 60 seconds.
	private _targets = _firer nearTargets 500;

	// For every target that isn't on our side, report them to all other nearby group leaders.
	{
		_x params ["_position", "_type", "_perceivedTargetSide", "_cost", "_target", "_accuracy"];
		if (isPlayer _target && {side _target != side _firer} && {_accuracy <= 125}) then {
			private _hasCalledForBackup = (group _leader) getVariable ["HAL_radio_calledForBackup", false];
			private _nearestPatrolGroup = [position _leader] call HAL_radio_fnc_nearestInactivePatrolGroup;
			
			// Move nearest non-COMBAT patrol group to support, if this group hasn't called for backup already.
			if (!_hasCalledForBackup && !isNull _nearestPatrolGroup) then {
				systemChat format["RADIO: %1 calling for backup from %2", group _leader, _nearestPatrolGroup];
				
				// Estimate a location based on our knowledge and send the nearest patrol group to there on a SAD mission.		
				private _estimatedLocation = [_target, _accuracy] call HAL_radis_fnc_getEstimatedTargetPosition;
				private _wp = [_nearestPatrolGroup, _estimatedLocation] call HAL_waypoints_fnc_createSadWaypoint;
				_nearestPatrolGroup setCurrentWaypoint _wp;

				// Prevent this group, or the group we called from calling for backup.
				(group _leader) setVariable ["HAL_radio_calledForBackup", true, true];
				_nearestPatrolGroup setVariable ["HAL_radio_calledForBackup", true, true];
			};

			// For every AI group:
			{
				// Reveal some information on enemy targets to group leaders within HAL_RADIO_DISTANCE.
				if (leader _x != _leader && {((leader _x) distance2D _leader) < HAL_RADIO_DISTANCE}) then {
					(leader _x) reveal [_target, 1];
					// systemChat format["RADIO: %1 revealed to group %2", _target, _x];
				};

				// Change any groups within HAL_COMBAT_DISTANCE to COMBAT mode.
				if (leader _x != _leader && {((leader _x) distance2D _leader) < HAL_COMBAT_DISTANCE}) then {
					_x setBehaviour "COMBAT";
					// systemChat format["RADIO: Group %1 set to combat mode due to nearby contact", _x];
				};

			} forEach HAL_groups;

			(group _leader) setVariable ["HAL_radio_lastReport", time, true];
		};
	} forEach _targets;
};
