// Ignore all normal clients.

if (hasInterface) exitWith {};

// Everything beyond here is a headless client.

[] call compile preProcessFileLineNumbers "HAL\initServer.sqf";

// Create a global variable to signify that we're using HC for zones.

HAL_headlessClientEnabled = true;
publicVariable "HAL_headlessClientEnabled";