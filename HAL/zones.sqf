HAL_zone_fnc_localityCheck = {
	// Returns true if the server/client should handle AI, false if it shouldn't.
	// Doing this check on each zone function allows for headless client to be handled without need for mission params.
	// As a result of this, all triggers should NOT be 'Server only'.

	// First, always return true if we're the server client in singleplayer or local multiplayer.
	if ((!isMultiplayer || is3DENMultiplayer) && isServer) exitWith { true };

	// Otherwise, ignore all normal clients.
	if (hasInterface) exitWith { false };

	// If a headless client exists on the server, we only want to allow headless clients to handle AI.
	// This variable will generally be set by the time a trigger is called,
	// as AI shouldn't be triggered until after briefing, and it is set on headless client init.
	if (HAL_headlessClientEnabled) exitWith {
		private _isHeadlessClient = !hasInterface && !isDedicated;
		_isHeadlessClient
	};

	// Otherwise, we only allow the server to handle AI.
	if (isDedicated) exitWith { true };
};

HAL_zone_fnc_initZone = {
	params ["_trigger", "_type"];

	_trigger setVariable ["HAL_zone_type", _type];
	HAL_zones pushBack _trigger;
};

HAL_zone_fnc_patrolGroup = {
	params ["_groupType", "_waypoints"];

	// Select a random starting point.
	private _spawnPos = selectRandom _waypoints;
	_waypoints = _waypoints - [_spawnPos];

	// Spawn a group with a random type.
	private _grp = [_spawnPos, _groupType, "PATROL"] call HAL_units_fnc_spawnGroup;

	// Assign the rest of the waypoints, then CYCLE at the spawn point.
	{
		[_grp, _x] call HAL_waypoints_fnc_createPatrolWaypoint;
	} forEach _waypoints;
	[_grp, _spawnPos] call HAL_waypoints_fnc_createCycleWaypoint;
};

HAL_zone_fnc_patrolZone = {
	private _validLocality = [] call HAL_zone_fnc_localityCheck;
	if (!_validLocality) exitWith {};

	params ["_trigger", "_infGroupCount", "_motorisedGroupCount", "_tankGroupCount"];

	[_trigger, "PATROL"] call HAL_zone_fnc_initZone;

	_trigger setVariable ["HAL_zone_type", "PATROL"];

	private _numberOfWaypoints =
		_infGroupCount * 4
		+
		_motorisedGroupCount * 4
		+
		_tankGroupCount * 4;

	private _waypointPool = [_trigger, _numberOfWaypoints] call HAL_waypoints_fnc_generateWaypoints;
	
	if (count _waypointPool == 0) exitWith {
		systemChat format["Patrol Zone %1 failed to initialise due to failed waypoint generation.", _trigger];
	};

	for "_i" from 1 to _infGroupCount do {
		private _waypoints = _waypointPool select [0, 4];
		_waypointPool deleteRange [0, 4];

		private _groupType = selectRandom HAL_INF_GROUPS;
		[_groupType, _waypoints] call HAL_zone_fnc_patrolGroup;
	};

	for "_i" from 1 to _motorisedGroupCount do {
		private _waypoints = _waypointPool select [0, 4];
		_waypointPool deleteRange [0, 4];
		
		private _groupType = selectRandom HAL_MOT_GROUPS;
		[_groupType, _waypoints] call HAL_zone_fnc_patrolGroup;
	};

	for "_i" from 1 to _tankGroupCount do {
		private _waypoints = _waypointPool select [0, 4];
		_waypointPool deleteRange [0, 4];
		
		private _groupType = selectRandom HAL_TNK_GROUPS;
		[_groupType, _waypoints] call HAL_zone_fnc_patrolGroup;
	};
};

HAL_zone_fnc_openDoors = {
	params ["_building"];

	private _doors = "getText (_x >> 'displayName') == 'Open door'" configClasses (configFile >> "CfgVehicles" >> typeOf _building >> "UserActions");

	{
		[_building, _forEachIndex, 1] call BIS_fnc_Door;
	} forEach _doors;
};

HAL_zone_fnc_buildingFiller = {
	private _validLocality = [] call HAL_zone_fnc_localityCheck;
	if (!_validLocality) exitWith {};

	params ["_building", "_percentageFilled"];

	private _groupType = selectRandom HAL_INF_GROUPS;

	// Open all doors in the building to prevent AI walking through doors.
	[_building] call HAL_zone_fnc_openDoors;

	private _groupUnitTypes = [_groupType] call HAL_units_fnc_getUnitsFromGroupConfig;

	private _spawnPos = [position _building, 0, 100, 5, 0, 0.4] call BIS_fnc_findSafePos;

	private _validPositions = [_building] call HAL_waypoints_fnc_findBuildingPositions;

	private _nGuards = floor((count _validPositions) * (_percentageFilled / 2));
	private _nPatrollers = ceil((count _validPositions) * (_percentageFilled / 2) * 0.5);

	for "_i" from 1 to _nGuards do {
		private _randomBuildingPosition = selectRandom _validPositions;
		_validPositions = _validPositions - [_randomBuildingPosition];

		private _unit = [_groupType, _randomBuildingPosition] call HAL_units_fnc_spawnCqcUnitFromGroupConfig;
	
		_unit disableAI "PATH";
		_unit setUnitPos "UP";
		_unit setDir (random 360);

		private _wp = [(group _unit), getPosATL _unit, 'AWARE', 'RED', "COLUMN", "LIMITED", "GUARD"] call HAL_waypoints_fnc_createWaypoint;
		(group _unit) setCurrentWaypoint _wp;
	};

	for "_i" from 1 to _nPatrollers do {
		private _randomBuildingPosition = selectRandom _validPositions;
		private _randomPatrolPosition = selectRandom _validPositions;
		_validPositions = _validPositions - [_randomBuildingPosition] - [_randomPatrolPosition];

		private _unit = [_groupType, _randomBuildingPosition] call HAL_units_fnc_spawnCqcUnitFromGroupConfig;
		_unit setUnitPos "UP";
		_unit setDir (random 360);

		private _wp = [(group _unit), getPosATL _unit] call HAL_waypoints_fnc_createPatrolWaypoint;
		(group _unit) setCurrentWaypoint _wp;

		[(group _unit), _randomPatrolPosition] call HAL_waypoints_fnc_createPatrolWaypoint;
		[(group _unit), getPosATL _unit] call HAL_waypoints_fnc_createCycleWaypoint;
	};
};

HAL_zone_fnc_garrisonGroup = {
	params ["_groupType", "_building"];

	// Open all doors in the building to prevent AI walking through doors.
	[_building] call HAL_zone_fnc_openDoors;

	private _groupUnitTypes = [_groupType] call HAL_units_fnc_getUnitsFromGroupConfig;

	private _spawnPos = [position _building, 0, 100, 5, 0, 0.4] call BIS_fnc_findSafePos;

	private _validPositions = [_building] call HAL_waypoints_fnc_findBuildingPositions;

	private _nGuards = floor((count _validPositions) / 4) min floor(count _groupUnitTypes / 2);
	private _nPatrollers = ceil((count _validPositions) / 4) min ceil(count _groupUnitTypes / 2);

	systemChat format["%1 guard, %2 patrols", _nGuards, _nPatrollers];

	for "_i" from 1 to _nGuards do {
		private _randomBuildingPosition = selectRandom _validPositions;
		_validPositions = _validPositions - [_randomBuildingPosition];

		private _unit = [_groupType, _randomBuildingPosition] call HAL_units_fnc_spawnCqcUnitFromGroupConfig;
		_unit disableAI "PATH";

		private _wp = [(group _unit), getPosATL _unit, 'SAFE', 'RED', "COLUMN", "LIMITED", "GUARD"] call HAL_waypoints_fnc_createWaypoint;
		(group _unit) setCurrentWaypoint _wp;
	};

	for "_i" from 1 to _nPatrollers do {
		private _randomBuildingPosition = selectRandom _validPositions;
		private _randomPatrolPosition = selectRandom _validPositions;
		_validPositions = _validPositions - [_randomBuildingPosition] - [_randomPatrolPosition];

		private _unit = [_groupType, _randomBuildingPosition] call HAL_units_fnc_spawnCqcUnitFromGroupConfig;

		private _wp = [(group _unit), getPosATL _unit] call HAL_waypoints_fnc_createPatrolWaypoint;
		(group _unit) setCurrentWaypoint _wp;

		[(group _unit), _randomPatrolPosition] call HAL_waypoints_fnc_createPatrolWaypoint;
		[(group _unit), getPosATL _unit] call HAL_waypoints_fnc_createCycleWaypoint;
	};
};

HAL_zone_fnc_garrisonZone = {
	private _validLocality = [] call HAL_zone_fnc_localityCheck;
	if (!_validLocality) exitWith {};

	params ["_trigger", "_infGroupCount"];

	[_trigger, "GARRISON"] call HAL_zone_fnc_initZone;

	private _triggerRadius = (triggerArea _trigger) select 0;

	private _buildingsInZone = nearestObjects [position _trigger, ["House", "Building"], _triggerRadius];

	private _spawnedGroups = 0;
	while {_spawnedGroups < _infGroupCount && {count _buildingsInZone > 0}} do {
		private _randomBuilding = selectRandom _buildingsInZone;

		if ([_randomBuilding] call BIS_fnc_isBuildingEnterable) then {
			private _validPositions = [_randomBuilding] call HAL_waypoints_fnc_findBuildingPositions;
			if (count _validPositions > 3) then {
				[selectRandom HAL_INF_GROUPS, _randomBuilding] call HAL_zone_fnc_garrisonGroup;
				_spawnedGroups = _spawnedGroups + 1;
			};
		};

		_buildingsInZone = _buildingsInZone - [_randomBuilding];
	};

	if (_spawnedGroups < _infGroupCount) exitWith {
		systemChat format["Garrison Zone %1 failed to initialise due to a lack of buildings in the area.", _trigger];
	};
};

HAL_zone_fnc_campWaveLoop = {
	params ["_trigger", "_groupTypes", "_groupWaveConfig", "_wpUnits"];
	_groupWaveConfig params ["_groupsPerWave", "_maxActiveGroups", "_waveDelaySeconds"];

	private _activeGroups = [];
	waitUntil {
		systemChat format["Waiting %1 seconds for camp %2.", _waveDelaySeconds, _trigger];
		sleep (_waveDelaySeconds);

		// Remove any groups that have less than half their original count.
		{
			private _alive = {alive _x} count (units _x);
			private _originalCount = _x getVariable ["HAL_man_count", 0];
			if (_alive <= (_originalCount / 2)) then {
				systemChat format["Removing dead group %1 for camp %2 (%3 <= %4). %5 active groups", _x, _trigger, _alive, _originalCount / 2, count _activeGroups];
				_activeGroups = _activeGroups - [_x];
			};
		} forEach _activeGroups;

		if (count _activeGroups < _maxActiveGroups) then {
			// Spawn either the requested groupsPerWave, or the difference between maxActiveGroups and currentlyActiveGroups.
			// Whichever is smaller.
			private _numberOfGroupsToSpawn = _groupsPerWave min (_maxActiveGroups - count _activeGroups);
			systemChat format["Spawning %1 groups for camp %2.", _numberOfGroupsToSpawn, _trigger];

			private _spawns = [_trigger, _numberOfGroupsToSpawn * 3] call HAL_waypoints_fnc_generateWaypoints;
			if (count _spawns < _groupsPerWave) exitWith {
				systemChat format["Camp wave %1 - %2 failed to initialise due to failed waypoint generation.", _trigger, _groupWaveConfig];
			};

			for "_i" from 1 to _numberOfGroupsToSpawn do {
				private _group = [selectRandom _spawns, selectRandom _groupTypes, "CAMP"] call HAL_units_fnc_spawnGroup;

				// Select a random waypoint path.
				private _wpUnit = selectRandom _wpUnits;

				// Copy all waypoints from the waypoint unit to the spawned camp group.
				_group copyWaypoints (group _wpUnit);

				// Add a final waypoint as a SAD waypoint on the last waypoint in the path.
				private _waypoints = waypoints _group;
				private _lastWp = _waypoints select (count _waypoints - 1);
				[_group, getWPPos _lastWp] call HAL_waypoints_fnc_createSadWaypoint;

				_activeGroups pushBack _group;
			};
		};

	false
	};
};

HAL_zone_fnc_campZone = {
	private _validLocality = [] call HAL_zone_fnc_localityCheck;
	if (!_validLocality) exitWith {};

	// Config format: [groupsPerWave, maxActiveGroups, waveDelaySeconds]
	params ["_trigger", "_infConfig", "_motorisedConfig", "_tankConfig"];

	[_trigger, "CAMP"] call HAL_zone_fnc_initZone;

	// Get all of the waypoint paths for this camp.
	private _radius = (triggerArea _trigger) select 0;
	private _wpUnits = nearestObjects [position _trigger, ["C_HAL_Waypoint_Unit"], _radius, true];
	systemChat format["Camp %1 has %2 paths", _trigger, count _wpUnits];

	if (count _infConfig > 0) then {
		[_trigger, HAL_INF_GROUPS, _infConfig, _wpUnits] spawn HAL_zone_fnc_campWaveLoop; 
	};
	
	if (count _motorisedConfig > 0) then {
		[_trigger, HAL_MOT_GROUPS, _motorisedConfig, _wpUnits] spawn HAL_zone_fnc_campWaveLoop; 
	};

	if (count _tankConfig > 0) then {
		[_trigger, HAL_TNK_GROUPS, _tankConfig, _wpUnits] spawn HAL_zone_fnc_campWaveLoop; 
	};
};