// Set globals.
HAL_zones = [];
HAL_groups = [];

// Precompile functions.
[] call compile preprocessFileLineNumbers "HAL\radio.sqf";
[] call compile preprocessFileLineNumbers "HAL\config.sqf";
[] call compile preprocessFileLineNumbers "HAL\debug.sqf";
[] call compile preprocessFileLineNumbers "HAL\units.sqf";
[] call compile preprocessFileLineNumbers "HAL\waypoints.sqf";
[] call compile preprocessFileLineNumbers "HAL\zones.sqf";

// Start debug marker loop if we're in local multiplayer / singleplayer.
if (!isMultiplayer || is3DENMultiplayer) then {
	[] spawn HAL_debug_fnc_aiMarkerLoop;
};
