KK_fnc_arrayFlatten = {
    private ["_res", "_fnc"];
    _res = [];
    _fnc = {
        {
            if (typeName _x isEqualTo "ARRAY") then [
                {_x call _fnc; false},
                {_res pushBack _x; false}
            ];
        } count _this;
    };
    _this call _fnc;
    _res
};

HAL_units_fnc_getAllGroups = {
	params ["_currentConfig"];

	if (isClass (_currentConfig >> "Unit0")) exitWith { _currentConfig };

	private _configChildren = "configName _x != 'Empty'" configClasses _currentConfig;

	private _results = [];
	{
		_results pushBack ([_x] call HAL_units_fnc_getAllGroups);
	} forEach _configChildren;

_results call KK_fnc_arrayFlatten
};

HAL_units_fnc_printAllGroups = {
	private _groupConfigs = [configFile >> "CfgGroups"] call HAL_units_fnc_getAllGroups;

	{
		private _hierarchy = [];
		{ _hierarchy pushBack format["'%1'", configName _x] } forEach (configHierarchy _x);

		private _humanReadable = (_hierarchy select [1, count _hierarchy - 1]) joinString " >> ";
		diag_log format["configFile >> %1", _humanReadable];
	} forEach _groupConfigs;
};

HAL_units_fnc_initGroup = {
	params ["_grp", "_type"];

	// Prep units within group.
	{
		[_x] call HAL_units_fnc_prepUnit;
	} forEach (units _grp);

	private _uniqueGrpId = format["%1_%2", side (leader _grp), groupId _grp];
	_grp setVariable ["HAL_groupId", _uniqueGrpId, true];

	_grp setVariable ["HAL_groupType", _type, true];
	_grp setVariable ["HAL_man_count", count units _grp, true];

	// Add radio handler on the leader of the group.
	(leader _grp) addEventHandler ["FiredNear", HAL_radio_fnc_firedNearHandler];

	HAL_groups pushBack _grp;
};

HAL_units_fnc_prepUnit = {
	params ["_unit"];

	// Remove NVGs, doing a search for any item with 'NVG' in the name.
	{
		if (["NVG", _x, true] call BIS_fnc_inString) exitWith {
			_unit unassignItem _x;
			_unit removeItem _x;
		};
	} forEach (assignedItems _unit);
};

HAL_units_fnc_getSideFromGroupConfig = {
	params ["_groupConfig"];

	private _sideId = (_groupConfig >> 'side') call BIS_fnc_getCfgData;
	private _side = _sideId call BIS_fnc_sideType;

_side
};

HAL_units_fnc_getUnitsFromGroupConfig = {
	params ["_groupConfig"];

	private _units = [];
	{
		_units pushBack ((_x >> 'vehicle') call BIS_fnc_getCfgData);
	} forEach ("true" configClasses _groupConfig);

_units
};

HAL_units_fnc_spawnCqcUnitFromGroupConfig = {
	params ["_groupConfig", "_position"];

	private _unitTypes = [_groupConfig] call HAL_units_fnc_getUnitsFromGroupConfig;
	private _groupSide = [_groupConfig] call HAL_units_fnc_getSideFromGroupConfig;

	private _grp = createGroup _groupSide;
	_grp enableAttack false;

	private _unit = _grp createUnit [selectRandom _unitTypes, _position, [], 0, "NONE"];
	_unit setPosATL _position;
	_unit setUnitPos "UP";
	_unit setDir (random 360);
	
	[_grp, "GARRISON"] call HAL_units_fnc_initGroup;
_unit
};

HAL_units_fnc_fillVehicles = {
	params ["_vehicles", "_uncrewedUnits"];

	{
		private _vehicle = _x;

		// Fill the vehicle crew, not using the extra units available.
		// This only fills empty commander/gunner/driver slots, and is a fallback in case of dodgy cfgGroups.
		createVehicleCrew _vehicle;

		// Then, fill up any FFV turrets with uncrewed units.
		_ffvTurrets = allTurrets [_vehicle, true] - allTurrets [_vehicle, false];
		{
			if (count _uncrewedUnits > 0) then {
				private _turretArray = _x;
				private _uncrewedUnit = _uncrewedUnits #0;
				_uncrewedUnit assignAsTurret [_vehicle, _turretArray];
				_uncrewedUnit moveInTurret [_vehicle, _turretArray];
				_uncrewedUnits = _uncrewedUnits - [_uncrewedUnit];
			};
		} forEach _ffvTurrets;

		// Then, fill the cargo seats with uncrewed units.
		private _cargoSlots = fullCrew [_vehicle, "cargo", true];
		{
			_x params ["_crew", "_role", "_cargoIndex", "_turretPath"];
			if (isNull _crew && {count _uncrewedUnits > 0}) then {
				private _uncrewedUnit = _uncrewedUnits #0;
				_uncrewedUnit assignAsCargo _vehicle;
				_uncrewedUnit moveInCargo _vehicle;
				_uncrewedUnits = _uncrewedUnits - [_uncrewedUnit];
			};
		} forEach _cargoSlots;

		// Set an appropriate leader.
		(group (effectiveCommander  _vehicle)) selectLeader (effectiveCommander  _vehicle);
	} forEach _vehicles;

	// Delete remaining uncrewed units.
	{
		deleteVehicle _x;
	} forEach _uncrewedUnits;
};

HAL_units_fnc_spawnGroup = {
	params ["_position", "_groupConfig", "_type"];

	private _side = [_groupConfig] call HAL_units_fnc_getSideFromGroupConfig;

	private _grp = [
		_position,
		_side,
		_groupConfig
	] call BIS_fnc_spawnGroup;

	_grp deleteGroupWhenEmpty true;

	// Get a list of all vehicles in group.
	private _vehicles = [];
	private _uncrewedUnits = [];
	{
		if (vehicle _x != _x) then {
			_vehicles pushBack (vehicle _x);
		} else {
			_uncrewedUnits pushBack _x;
		};
	} forEach (units _grp);

	// With motorized/mechanised groups, we need to make sure vehicles are manned, with no infantry leftover.
	if (count _vehicles > 0) then {
		[_vehicles, _uncrewedUnits] call HAL_units_fnc_fillVehicles;
	};

	// Configure default behaviour and formations.
	_grp setFormation (selectRandom HAL_VALID_FORMATIONS);
	_grp setBehaviour "AWARE";
	_grp setCombatMode "YELLOW";
	_grp setSpeedMode "NORMAL";

	[_grp, _type] call HAL_units_fnc_initGroup;

_grp
};