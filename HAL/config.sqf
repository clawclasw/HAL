HAL_VALID_FORMATIONS = [
	"COLUMN",
	"WEDGE",
	"ECH LEFT",
	"ECH RIGHT",
	"LINE"
];

HAL_INF_GROUPS = [
	configFile >> 'CfgGroups' >> 'East' >> 'rhsgref_faction_chdkz' >> 'rhsgref_group_chdkz_insurgents_infantry' >> 'rhsgref_group_chdkz_insurgents_squad'
];

HAL_MOT_GROUPS = [
	configFile >> 'CfgGroups' >> 'East' >> 'rhsgref_faction_chdkz' >> 'rhs_group_indp_ins_gaz66' >> 'rhs_group_chdkz_gaz66_squad'
];

HAL_TNK_GROUPS = [
	configFile >> 'CfgGroups' >> 'East' >> 'rhsgref_faction_chdkz' >> 'rhs_group_indp_ins_btr70' >> 'rhs_group_chdkz_btr70_squad'
];